package com.example.davaleba14

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn

class NewsViewModel : ViewModel() {

    private val retroRepo = NewsRepository()

    fun fetchNews(): LiveData<PagingData<NewsModel>> {
        return retroRepo.getNews().cachedIn(viewModelScope)
    }
}
