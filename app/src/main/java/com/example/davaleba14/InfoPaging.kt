package com.example.davaleba14


import androidx.paging.PagingSource
import androidx.paging.PagingState
import retrofit2.HttpException
import java.io.IOException

private const val STARTING_PAGE_INDEX = 1

class InfoPaging: PagingSource<Int,NewsModel >() {
    override fun getRefreshKey(state: PagingState<Int, NewsModel>): Int? {
        TODO("Not yet implemented")
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, NewsModel> {
        val position = params.key ?: STARTING_PAGE_INDEX
        return try {
            val response = RetrofitService.retrofit().getRequest(ApiName.news,position)
            val data = response.body()!!
            LoadResult.Page(
                data = data,
                prevKey = if (position == STARTING_PAGE_INDEX) null else position,
                nextKey = if (data.isEmpty()) null else position + 1
            )
        } catch (exception: IOException) {
            return LoadResult.Error(exception)
        } catch (exception: HttpException) {
            return LoadResult.Error(exception)
        }
    }
}
