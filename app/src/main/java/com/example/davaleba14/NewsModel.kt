package com.example.davaleba14

data class NewsModel (
    val id: Int = 0,
    val titleKA: String = "",
    val cover: String = "",
    var isLast:Boolean = false
    )
