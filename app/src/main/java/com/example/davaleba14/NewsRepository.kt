package com.example.davaleba14

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.liveData

class NewsRepository {

    companion object {
        private const val NETWORK_PAGE_SIZE = 10
    }

    fun getNews(): LiveData<PagingData<NewsModel>> {
        Log.d("PixaBayRepository", "New page")
        return Pager(
            config = PagingConfig(
                pageSize = NETWORK_PAGE_SIZE,
                enablePlaceholders = true
            ),
            pagingSourceFactory = { InfoPaging() }
        ).liveData
    }

}